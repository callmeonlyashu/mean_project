'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

  var oauth = require('oauth');

  var yahooFinance = require('yahoo-finance');



/**
 * Create a Stock
 */
exports.create = function (req, res) {

};

/**
 * Show the current Stock
 */
exports.read = function (req, res) {

};

/**
 * Update a Stock
 */
exports.update = function (req, res) {

};

/**
 * Delete an Stock
 */
exports.delete = function (req, res) {

};

/**
 * List of Stocks
 */
exports.list = function (req, res) {

};

exports.displayStock = function(req, res){

	//console.log(req.query);
	var cmpnyName = req.query.cmpnySelect;
	var from = req.query.from;
	var to = req.query.to;

	yahooFinance.historical({
	  symbol: cmpnyName,
	  from: from,
	  to: to,
	  // period: 'd'  // 'd' (daily), 'w' (weekly), 'm' (monthly), 'v' (dividends only)
	}, function (err, quotes) {
	  //...
	  //console.log(quotes);
	  res.json(quotes);
	});


     
	yahooFinance.snapshot({
	  symbol: cmpnyName,
	  //fields: ['s', 'n', 'd1', 'l1', 'y', 'r'],
	}, function (err, snapshot) {
	  //...
	  //console.log(snapshot);
	  //res.json(snapshot);
	});

};
