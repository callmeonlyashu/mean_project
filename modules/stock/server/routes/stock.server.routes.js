'use strict';

module.exports = function(app) {
  // Routing logic   
  // ...
var stock = require('../controllers/stock.server.controller');

  app.route('/api/stock').get(stock.displayStock);
};
