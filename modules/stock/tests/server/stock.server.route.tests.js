'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  express  = require(path.resolve('./config/lib/express'));

  var dateFormat = require('dateformat');
  

/**
 * Globals
 */
var app,agent,stockData,stockEmptyData,stockValidData,stockValidRecords,numberOfData;

//var to = dateFormat('2017-02-17');
//var fromDate = dateFormat('2017-02-01');
/**
 * Unit tests
 */
describe('Stock Route tests:', function() {
  
  before(function(done){
    app = express.init(mongoose);
    agent  = request.agent(app);
    done();    
  });

  beforeEach(function(done) {
    stockData = {cmpnySelect:'GOOGL',from:'2017-02-01',to:'2017-02-17'};
    done();
  });

  describe('Stock Route tests for valid data', function() {
    
//    it('should not get response for empty data', function(done) {
//      agent.get('/api/stock')
//        .query({cmpnySelect:'',to:'',from:''})
//        .expect(302)
//        .end(function(reqErr,res){
//          if(reqErr){
//            return done(reqErr);
//          }
//
//          done();
//        });
//    });

//    it('should not get response for invalid data', function(done) {
//      agent.get('/api/stock')
//        .query({cmpnySelect:'GOOGL',to:'2017-02-04',from:'2017-02-07'})
//        .expect(302)
//        .end(function(reqErr,res){
//          if(reqErr){
//            return done(reqErr);
//          }
//          done();
//        });
//    });

    it('should get response for valid data', function(done) {
      agent.get('/api/stock')
        .query(stockData)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(reqErr,resData){
          if(reqErr){
            return done(reqErr);
          }
          var data = resData.res.body;
          (data[0].symbol).should.match(stockData.cmpnySelect);

          var d1=new Date(stockData.from);
          var d2=new Date(stockData.to);
          d2 = d2.setDate(d2.getDate()+1);

          for(var i=0; d1<d2; ) {
            if( d1.getDay() >0 && d1.getDay() <6) i++;
                d1.setDate(d1.getDate()+1) ;
          }
          
          (data.length).should.equal(i);

          done();
        });
    });

  });

  afterEach(function(done) {
    done();
  });
});
