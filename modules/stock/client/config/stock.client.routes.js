(function () {
  'use strict';

  //Setting up route
  angular
    .module('stock')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    // Stock state routing
    $stateProvider
      .state('stock', {
        url: '/stock',
        templateUrl: 'modules/stock/client/views/stock.client.view.html',
        controller: 'StockController',
        controllerAs: 'vm'
      });
  }
})();
