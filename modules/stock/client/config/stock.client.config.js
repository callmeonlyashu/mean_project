(function() {
  'use strict';

  // Stock module config
  angular
    .module('stock')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Config logic
    Menus.addMenuItem('topbar',{
      title:'Stock',
      state:'stock',
      roles:['*']
    });
  }
})();
