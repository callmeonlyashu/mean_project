(function() {
  'use strict';

  angular
    .module('stock')
    .controller('StockController', StockController);

  StockController.$inject = ['$scope','$http'];

  function StockController($scope,$http) {
    var vm = this;

    // Stock controller logic

    $scope.cmpnyName  = ['GOOGL', 'TWTR', 'FB','AAPL','LNKD','NFLX','TSLA','LAM','MSFT','AMZN'];

    /*$http.get('/api/stock').then(function(data){
          
          //console.log(data);

          $scope.symbol = data.data.symbol;
          $scope.name = data.data.name;
          $scope.lastTradeDate = data.data.lastTradeDate;
          $scope.lastTradePriceOnly = data.data.lastTradePriceOnly;  
          $scope.peRatio = data.data.peRatio;
          $scope.dividendYield = data.data.dividendYield; 

    });
*/
    $scope.stockPrice = function(){

        var config= {
          params:{
            from:$scope.from,
            to:$scope.to,
            cmpnySelect:$scope.cmpnySelect  
          }
        };

        $http.get('/api/stock',config).then(function(res){

          $scope.stockData = res.data;

          var date = [] ;
          var high = [];
          var low = [];
          var open = [];
          var close = [];

          angular.forEach(res.data, function(value, key) {
            date.push(value.date);
            high.push(value.high);
            low.push(value.low);
            open.push(value.open);
            close.push(value.close);
          });

          var options = {
                height:200,
                type: 'line',
                data: {
                    labels: date,
                    datasets: [{
                            label: 'High',
                            data: high,
                            borderWidth: 1
                        },
                        {
                            label: 'Low',
                            data: low,
                            borderWidth: 1,
                        },
                        {
                            label: 'Open',
                            data: open,
                            borderWidth: 1
                        },
                        {
                            label: 'Close',
                            data: close,
                            borderWidth: 1
                        }]
                    },
                options: {
                scales: {
                    yAxes: [{
                        ticks: {
                                reverse: false
                            }
                        }]
                    }
                }
            };

           var ctx = document.getElementById('myChart').getContext('2d');
           new Chart(ctx, options);
 

        });

   

    };

    init();

    function init() {
    }

  }
})();
