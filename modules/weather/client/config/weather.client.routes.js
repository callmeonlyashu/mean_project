(function () {
  'use strict';

  //Setting up route
  angular
    .module('weather')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    // Weather state routing
    $stateProvider
      .state('weather', {
        url: '/weather',
        templateUrl: 'modules/weather/client/views/weather.client.view.html',
        controller: 'WeatherController',
        controllerAs: 'vm'
      });
  }
})();
