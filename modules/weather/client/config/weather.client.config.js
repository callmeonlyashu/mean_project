(function() {
  'use strict';

  // Weather module config
  angular
    .module('weather')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Config logic
    Menus.addMenuItem('topbar',{
        title:'Weather',
        state:'weather',
        roles:['*']
    });
  }
})();
