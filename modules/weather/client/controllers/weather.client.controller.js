(function() {
  'use strict';

  angular
    .module('weather')
    .controller('WeatherController', WeatherController);

  WeatherController.$inject = ['$scope','$http'];

  function WeatherController($scope,$http) {
    var vm = this;
    
    /*if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        console.log("Geolocation is not supported by this browser."); 
    }
    function showPosition(position) {
        var lat = position.coords.latitude;
        var long = position.coords.longitude;
        $http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+long+'&key=AIzaSyCfNH2qO2g4ARYzgQ2JNPmaq-4C70kGWts').success(function(data){
            $scope.getWeather(data.results['5'].formatted_address);
        });

    }*/
    // Weather controller logic
    $scope.getWeather = function(){
        
        var config = {
                params: {
                    city:$scope.city,
                    country:$scope.country
                }
            };

        $http.get('/api/weather',config).then(function(res){
            //console.log(res);
            $scope.url=res.data.query.results.channel.image.url;
            $scope.gcity=res.data.query.results.channel.location.city;
            $scope.gcountry=res.data.query.results.channel.location.country;
            $scope.gregion=res.data.query.results.channel.location.region;
            $scope.gdate=res.data.query.results.channel.item.condition.date;
            $scope.gtemp=res.data.query.results.channel.item.condition.temp;
            
            
            var data = res.data.query.results.channel.item.forecast;
            
            var options = {
                height:200,
                type: 'line',
                data: {
                    labels: [data[0].date, data[1].date, data[2].date, data[3].date, data[4].date, data[5].date,data[6].date,data[7].date,data[8].date,data[9].date],
                    datasets: [{
                            label: 'Highest expected temperature ',
                            data: [data[0].high, data[1].high, data[2].high, data[3].high, data[4].high, data[5].high,data[6].high,data[7].high,data[8].high,data[9].high],
                            borderWidth: 1
                        },
                        {
                            label: 'Lowest expected temperature',
                            data: [data[0].low, data[1].low, data[2].low, data[3].low, data[4].low, data[5].low,data[6].low,data[7].low,data[8].low,data[9].low],
                            borderWidth: 1,
                        }]
                    },
                options: {
              	scales: {
                    yAxes: [{
                        ticks: {
                                reverse: false
                            }
                        }]
                    }
                }
            };

            var ctx = document.getElementById('weatherChart').getContext('2d');
            new Chart(ctx, options);
            
        });
    };
    
    init();

    function init() {
    }
  }
})();
