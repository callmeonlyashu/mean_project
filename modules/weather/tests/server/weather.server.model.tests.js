'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Weather = mongoose.model('Weather');

/**
 * Globals
 */
var user,
  weather;

/**
 * Unit tests
 */
//describe('Weather Model Unit Tests:', function() {
//  beforeEach(function(done) {
//    user = new User({
//      firstName: 'Full',
//      lastName: 'Name',
//      displayName: 'Full Name',
//      email: 'test@test.com',
//      username: 'username',
//      password: 'password'
//    });
//
//    user.save(function() {
//      weather = new Weather({
//        // Add model fields
//        // ...
//      });
//
//      done();
//    });
//  });
//
//  describe('Method Save', function() {
//    it('should be able to save without problems', function(done) {
//      return weather.save(function(err) {
//        should.not.exist(err);
//        done();
//      });
//    });
//  });
//
//  afterEach(function(done) {
//    Weather.remove().exec();
//    User.remove().exec();
//
//    done();
//  });
//});
