'use strict';

var should = require('should'),
        request = require('supertest'),
        path = require('path'),
        mongoose = require('mongoose'),
        express = require(path.resolve('./config/lib/express'));

var app, agent, location;

describe('Weather Route test', function () {
    before(function (done) {
        app = express.init(mongoose);
        agent = request.agent(app);
        done();
    });

    beforeEach(function (done) {
        location = {
            city: 'Mumbai',
            country: 'India'
        };
        done();
    });

    it('should be able to get 10 days weather forcast data', function (done) {
        agent.get('/api/weather')
                .query(location)
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (locationErr, locationRes) {
                    if (locationErr) {
                        return  done(locationErr);
                    }
                    
                    var resData = locationRes.body;
                    var locData = resData.query.results.channel.location;
                    var forecast = resData.query.results.channel.item.forecast;
                    
                    (locData.city).should.match(location.city);
                    (locData.country).should.match(location.country);
                    (forecast.length).should.equal(10);
                    
                    var d = new Date();
                    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                    
                    var today = d.getDate()+' '+months[d.getMonth()]+' '+d.getFullYear();
                    (forecast[0].date).should.match(today);
                    
                    done();
                });
    });
});