'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
    mongoose = require('mongoose'),
    errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
    _ = require('lodash');

/**
 * Create a Weather
 */
exports.create = function (req, res) {

};

/**
 * Show the current Weather
 */
exports.read = function (req, res) {

};

/**
 * Update a Weather
 */
exports.update = function (req, res) {

};

/**
 * Delete an Weather
 */
exports.delete = function (req, res) {

};

/**
 * List of Weathers
 */
exports.list = function (req, res) {

};


exports.getWeatherReport = function (req, res) {
    var YQL = require('yql');
    var query = new YQL('select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="'+req.query.city+','+req.query.country+' in") and u="c" ');
    query.exec(function (err, data) {
        res.json(data);
    });
};
