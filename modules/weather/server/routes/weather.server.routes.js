'use strict';

module.exports = function(app) {
  // Routing logic   
  var weather = require('../controllers/weather.server.controller');
 
  app.route('/api/weather').get(weather.getWeatherReport);
};
